# Licenses fonticons

> A fonticon with licenses logos

This repository contains the generated font and SCSS file you should use.
If you need the sources, check [this repo](https://gitlab.com/experimentslabs/licenses-fonticon-sources).

**State of this project:** early days.

## Usage

Put the fonts in a folder of your choice on your project
Use the SASS file to use the font like this in html files:

```html
<i class="li li-cc-0"></i>
```

### Variants
You may use the following variants:

- `li-lg` for a slightly larger icon
- `li-2x`, `li-3x`, `li-4x` and `li-5x` for icons x times bigger that the
  current font size
- `li-fw` for fixed width icons (for now, every glyph have the same sizes)

## Contributing

If you want to contribute, check the [sources repository](https://gitlab.com/experimentslabs/licenses-fonticon-sources).

## Licenses

The font and scss files are licensed under the [Creative Commons Zero license](LICENSE.md).
That mean the font is public domain.

Every icon have their own licenses and should not be used individually if
the license does not allow it. Their individual licenses are listed in the
[sources repository](https://gitlab.com/experimentslabs/licenses-fonticon-sources).

If you represent any of the organization for which a logo has been created
in this font and have any legal issue with it, please contact us by opening
an issue on the [sources repository](https://gitlab.com/experimentslabs/licenses-fonticon-sources).
We'll be glad to find a good decision together. You can also contact me directly
on [Gitter](https://gitter.im/mtancoigne)

**NOTE**: All brand icons are trademarks of their respective owners. The use of
          these trademarks does not indicate endorsement of the trademark holder
          by Experiments Labs, nor vice versa. Please do not use these logos for any
          purpose except to represent the company, product, or service to which
          they refer.
